import {binding, component, injectable} from "~/util/angular-utils";
import {autobindAll} from "~/util/autobind";

const newTodoComponentOptions: ng.INamedComponentOptions = {
    registeredName: "newTodo",
    templateUrl: "dist/components/new-todo/new-todo.tpl.html",
    controllerAs: "$ctrl"
};

@injectable()
@autobindAll
class NewTodoController implements ng.IController {
    public newTodo: string = "";

    @binding("&")
    public onSubmit!: (params: {description: string}) => void;

    public onFormSubmit(): void {
        this.onSubmit({description: this.newTodo});
        this.newTodo = "";
    }
}

export default component(newTodoComponentOptions, NewTodoController);
