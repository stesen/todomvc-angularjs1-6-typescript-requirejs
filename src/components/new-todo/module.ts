import angular from "angular";

import {ngImports} from "~/util/angular-utils";
import newTodoComponent from "./new-todo-component";

export default angular.module("newTodo", ngImports())
    .component(newTodoComponent);
