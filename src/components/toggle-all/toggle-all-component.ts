import {binding, component, injectable} from "~/util/angular-utils";
import {autobindAll} from "~/util/autobind";

const toggleAllComponentOptions: ng.INamedComponentOptions = {
    registeredName: "toggleAll",
    templateUrl: "dist/components/toggle-all/toggle-all.tpl.html",
    controllerAs: "$ctrl"
};

@injectable()
@autobindAll
class ToggleAllController implements ng.IController {
    public allChecked: boolean = false;

    @binding("&")
    public onToggle!: (params: {checked: boolean}) => void;
}

export default component(toggleAllComponentOptions, ToggleAllController);
