import angular from "angular";

import {ngImports} from "~/util/angular-utils";
import toggleAllComponent from "./toggle-all-component";

export default angular.module("toggleAll", ngImports())
    .component(toggleAllComponent);
