import Todo from "~/models/todo";
import {TodoService} from "~/services/todos/todo-service";
import {binding, component, injectable} from "~/util/angular-utils";
import {autobindAll} from "~/util/autobind";

const todoComponentOptions: ng.INamedComponentOptions = {
    registeredName: "todo",
    templateUrl: "dist/components/todo/todo.tpl.html",
    controllerAs: "$ctrl"
};

@injectable(
    "$element",

    "TodoService"
)
@autobindAll
class TodoController implements ng.IController {
    private readonly editInput: JQLite;

    @binding("<")
    public todo!: Todo;

    public editing: boolean = false;
    public newDescription: string = "";

    constructor(
        $element: JQLite,

        private readonly todoService: TodoService
    ) {
        this.editInput = $element.find("input.edit");
    }

    public toggle(): void {
        this.todoService.editTodo(this.todo.id, {completed: !this.todo.completed});
    }

    public beginEdit(): void {
        this.newDescription = "";
        this.editing = true;
        this.editInput[0].focus();
    }

    public delete(): void {
        this.todoService.deleteTodo(this.todo.id);
    }

    public submitEdit(): void {
        this.todoService.editTodo(this.todo.id, {description: this.newDescription});
        this.editing = false;
    }

    public onKeyup(ev: KeyboardEvent): void {
        if (ev.key === "Escape") {
            this.editInput[0].blur();
            this.editing = false;
            this.newDescription = "";
        }
    }
}

export default component(todoComponentOptions, TodoController);
