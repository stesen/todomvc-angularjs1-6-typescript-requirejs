import angular from "angular";

import todoServiceModule from "~/services/todos/module";
import {ngImports} from "~/util/angular-utils";
import todoComponent from "./todo-component";

export default angular.module("todo", ngImports(
    todoServiceModule
))
    .component(todoComponent);
