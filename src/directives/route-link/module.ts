import angular from "angular";

import {ngImports} from "~/util/angular-utils";
import routeLinkDirective from "./route-link-directive";

export default angular.module("routeLink", ngImports())
    .directive(routeLinkDirective);
