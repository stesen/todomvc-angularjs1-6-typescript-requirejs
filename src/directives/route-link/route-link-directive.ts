import {
    directive,
    IDirectiveLifecycleController,
    IDirectiveOptions,
    injectable
} from "~/util/angular-utils";
import {autobindAll} from "~/util/autobind";

const routeLinkOptions: IDirectiveOptions = {
    registeredName: "routeLink",
    restrict: "A",
    scope: {},
    controllerAs: "$ctrl",
    priority: 1000,
    terminal: true
};

interface RouteLinkAttributes extends ng.IAttributes {
    routeLink: string;
}

@injectable(
    "$compile",
    "$location"
)
@autobindAll
class RouteLinkController
implements IDirectiveLifecycleController<RouteLinkController, RouteLinkAttributes>
{
    public get currentPath(): string {
        return ensureSlash(this.$location.path());
    }

    constructor(
        private readonly $compile: ng.ICompileService,
        private readonly $location: ng.ILocationService
    ) {}

    public $doCompile($tEl: JQLite, $tAttrs: RouteLinkAttributes): void {
        const route = $tAttrs.routeLink;
        const path = ensureSlash(route);
        const href = "#!" + path;
        $tAttrs.$set("href", href);
        $tAttrs.$set("ngClass", `{'selected': $ctrl.currentPath === '${path}'}`);
        $tAttrs.$set("routeLink", null);
    }

    public $link($scope: ng.IScope, $el: JQLite): void {
        this.$compile($el)($scope);
    }
}

function ensureSlash(route: string): string {
    return route[0] === "/" ? route : ("/" + route);
}

export default directive(routeLinkOptions, RouteLinkController);
