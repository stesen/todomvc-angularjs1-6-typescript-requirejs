import angular from "angular";

import newTodoModule from "~/components/new-todo/module";
import todoModule from "~/components/todo/module";
import toggleAllModule from "~/components/toggle-all/module";
import routeLinkModule from "~/directives/route-link/module";
import todoServiceModule from "~/services/todos/module";
import {ngImports} from "~/util/angular-utils";
import appComponent from "./app-component";

angular.module("app", ngImports(
    newTodoModule,
    todoModule,
    toggleAllModule,
    routeLinkModule,
    todoServiceModule
))
    .component(appComponent);

//Passing a function to angular.element is a shorthand for onDOMContentReady
angular.element(() => angular.bootstrap(document, ["app"], {strictDi: true}));
