import Todo from "~/models/todo";
import {TodoService} from "~/services/todos/todo-service";
import {component, injectable} from "~/util/angular-utils";
import {autobindAll} from "~/util/autobind";

const appComponentOptions: ng.INamedComponentOptions = {
    registeredName: "app",
    templateUrl: "dist/app-component.tpl.html",
    controllerAs: "$ctrl"
};

@injectable(
    "$location",
    "TodoService"
)
@autobindAll
class AppController implements ng.IController {
    public todos!: readonly Readonly<Todo>[];
    public completedCount!: number;
    public uncompletedCount!: number;

    constructor(
        private readonly $location: ng.ILocationService,
        private readonly todoService: TodoService
    ) {}

    public $onInit(): void {
        this.todoService.todos.subscribe(todos => this.todos = todos);
        this.todoService.completedCount.subscribe(count => this.completedCount = count);
        this.todoService.uncompletedCount.subscribe(count => this.uncompletedCount = count);
    }

    public addTodo(description: string): void {
        this.todoService.addTodo(description);
    }

    public markAll(completed: boolean): void {
        this.todoService.editTodos(this.todos.map(todo => [todo.id, {completed}]));
    }

    public clearCompleted(): void {
        this.todoService.deleteTodos(
            this.todos
                .filter(todo => todo.completed)
                .map(todo => todo.id)
        );
    }

    public todoIsVisible(todo: Todo): boolean {
        const route = this.$location.path();
        return !route || route === "/" || todo.completed === (route === "/completed");
    }
}

export default component(appComponentOptions, AppController);
