export default interface Todo {
    readonly id: string;
    description: string;
    completed: boolean;
}
