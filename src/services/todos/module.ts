import angular from "angular";
import "angular-localforage";

import {ngImports} from "~/util/angular-utils";
import todoService from "./todo-service";

export default angular.module("todoService", ngImports(
    "LocalForageModule"
))
    .service(todoService);
