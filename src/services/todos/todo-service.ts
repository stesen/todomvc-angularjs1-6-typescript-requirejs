import Todo from "~/models/todo";
import {injectable, service} from "~/util/angular-utils";
import {autobindAll} from "~/util/autobind";
import PatchableDataSet, {Diff} from "~/util/patchable-data-set";
import uuidv4 from "~/util/uuid";

import {concat, EMPTY, from, Observable} from "rxjs";
import {ignoreElements, map, scan, skip} from "rxjs/operators";

const LF_KEY = "todos";

@injectable(
    "$localForage"
)
@autobindAll
export class TodoService {
    public static readonly registeredName: string = "TodoService";

    private readonly dataSet = new PatchableDataSet<Todo, "id">("id");
    private initialized: boolean = false;

    public get todos(): Observable<readonly Readonly<Todo>[]> {
        return concat(this.ensureInitialized(), this.dataSet.observable);
    }

    public get completedCount(): Observable<number> {
        return this.watch("completed").pipe(
            scan((count, {after}) => after && after.completed ? count + 1 : count && count - 1, 0)
        );
    }

    public get uncompletedCount(): Observable<number> {
        return this.completedCount.pipe(map(completed => this.dataSet.size - completed));
    }

    constructor(
        private readonly $localForage: ng.localForage.ILocalForageService
    ) {
        this.dataSet.observable
            .pipe(skip(1)) //Skip the initial push on initialization
            .subscribe(todos => $localForage.setItem(LF_KEY, todos));
    }

    private ensureInitialized(): Observable<never> {
        if (this.initialized) return EMPTY;

        const initPromise = this.$localForage.getItem(LF_KEY).then((todos: Todo[] | null) =>
            this.dataSet.replaceAll(todos || [])
        );

        this.initialized = true;

        return from(initPromise).pipe(ignoreElements());
    }

    private createTodo(description: string): Todo {
        return {
            id: uuidv4(),
            description,
            completed: false
        };
    }

    public editTodos(patches: [string, Partial<Todo>][]): void {
        this.ensureInitialized().subscribe({complete: () => this.dataSet.edit(patches)});
    }

    public editTodo(id: string, patch: Partial<Todo>): void {
        this.editTodos([[id, patch]]);
    }

    public deleteTodos(ids: string[]): void {
        this.ensureInitialized().subscribe({complete: () => this.dataSet.delete(ids)});
    }

    public deleteTodo(id: string): void {
        this.deleteTodos([id]);
    }

    public addTodos(descriptions: string[]): void {
        this.ensureInitialized().subscribe({
            complete: () => this.dataSet.add(descriptions.map(this.createTodo))
        });
    }

    public addTodo(description: string): void {
        this.addTodos([description]);
    }

    public watch(prop?: keyof Todo): Observable<Diff<Todo>> {
        return concat(this.ensureInitialized(), this.dataSet.watch(prop));
    }
}

export default service(TodoService);
