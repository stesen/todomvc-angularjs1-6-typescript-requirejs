import {has} from "./object-utils";

function _autobind<T extends Function>(
    target: Object,
    key: PropertyKey,
    origMethod: T
): TypedPropertyDescriptor<T> {
    let method = origMethod;
    let bound = new WeakMap<any, T>();
    return {
        enumerable: true,
        get() {
            if (target.isPrototypeOf(this)) {
                if (!bound.has(this)) {
                    bound.set(this, method.bind(this));
                }

                return bound.get(this)!;
            }
            else return method;
        },
        set(val) {
            if (target.isPrototypeOf(this)) {
                this[key] = val;
            }
            else {
                method = val;
                bound = new WeakMap();
            }
        }
    };
}

/**
 * Decorator which causes a method to automatically be bound when referenced through an instance
 */
export function autobind<T extends Function>(
    target: Object,
    methodName: PropertyKey,
    {configurable, value: origMethod}: TypedPropertyDescriptor<T>
): TypedPropertyDescriptor<T> {
    if (typeof target === "function")
        throw new TypeError("Autobind cannot be used on static methods");
    if (!origMethod) throw new TypeError("Autobind cannot be used on getters");
    if (!configurable) throw new TypeError("Autobind cannot be used on non-configurable methods");

    return _autobind(target, methodName, origMethod);
}

/**
 * Class decorator which autobinds all enumerable, configurable, string-keyed methods in the class
 */
export function autobindAll(clazz: new (...params: any) => any): void {
    const proto = clazz.prototype;
    for (const key of Object.keys(proto)) {
        const descriptor = Object.getOwnPropertyDescriptor(proto, key)!;
        if (
            descriptor.configurable
            && has(descriptor, "value")
            && typeof descriptor.value === "function"
        ) {
            Object.defineProperty(proto, key, _autobind(proto, key, descriptor.value));
        }
    }
}
