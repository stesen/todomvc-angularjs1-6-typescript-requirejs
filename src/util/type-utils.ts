/**
 * Omits one or more propeties from a type
 *
 * @typeParam T - The type to omit properties from
 * @typeParam K - The property keys to omit
 */
export type Omit<T, K extends PropertyKey> = Pick<T, Exclude<keyof T, K>>;

/**
 * Like Partial, but asserts that the object is non-empty
 *
 * @typeParam T - The type to create a non-empty partial of
 */
//Thanks to https://stackoverflow.com/a/48244432
export type AtLeastOne<T> = Partial<T> & {[K in keyof T]: Pick<T, K>}[keyof T];

/**
 * Any primitive value
 */
export type Primitive = string | number | symbol | boolean;

export type ExtractKeys<T extends PropertyKey> =
    T extends any ? (
        string extends T ? never :
        number extends T ? never :
        symbol extends T ? never :
        T
    ) : never;

export type Require<T, K extends keyof T> = T & {[P in K]-?: T[P]};

/**
 * Ensures that a key is present on a type,
 * either by removing an optional marker, or adding a new property with type `unknown`
 *
 * @typeParam U - The type to ensure a property in
 * @typeParam K - The key of the property to ensure
 */
export type Ensure<U, K extends PropertyKey> =
    K extends keyof U ? Require<U, K> :
    K extends ExtractKeys<K> ? U & Record<K, unknown> :
    U;
