//https://stackoverflow.com/a/2117523/4913931
/**
 * An implementation of the UUID algorithm
 *
 * @returns A new UUID
 */
export default function uuidv4(): string {
    return `${1e7}-${1e3}-${4e3}-${8e3}-${1e11}`.replace(/[018]/g, c => {
        const n = Number(c);
        return (n ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> n / 4).toString(16);
    });
}
