import {Ensure} from "./type-utils";

/**
 * Performs a shallow-equals operation on two objects
 *
 * @param a - The first object to compare
 * @param b - The second object to compare
 *
 * @returns True if all properties on the two objects are equal with the === operator
 */
export function shallowEquals(a: object | null | undefined, b: object | null | undefined): boolean {
    if (a === b) return true;
    if (!a || !b) return false;

    const aKeys = Object.keys(a);
    const bKeys = Object.keys(b);

    if (aKeys.length !== bKeys.length) return false;

    for (const key of aKeys) {
        if (!(key in b) || a[key] !== b[key]) return false;
    }

    return true;
}

/**
 * Finds the key of a given value in an object, if it exists
 *
 * @param obj - The object to search in
 * @param val - The value to search for
 *
 * @returns The key of the value, if it was found, else undefined
 */
export function keyOf(obj: object, val: unknown): string | undefined {
    for (const key of Object.keys(obj)) {
        if (obj[key] === val) return key;
    }

    return undefined;
}

//TypeScript doesn't naturally treat in and hasOwnProperty as proving that
//the given property name exists in the object. The below functions implement this behavior.
//They're not perfect (generics can make them give false positives),
//but no dynamic code of this variety (accessing arbitrary properties on an unknown object)
//ever will be.

//Thanks to https://github.com/Microsoft/TypeScript/issues/21732

/**
 * Checks if a key is present in an object, possibly through inheritance.
 *
 * @remarks
 * This function serves as a type guard which asserts the presence of the key, if found
 *
 * @typeParam T - The type of the object being searched
 * @typeParam K - The key being searched for
 *
 * @param o - The object being searched
 * @param k - The key being searched for
 *
 * @returns True if the key was found
 */
export function has<T, K extends PropertyKey>(o: T, k: K): o is Ensure<T, K> {
    return k in o;
}

/**
 * Checks if an own key is present in an object
 *
 * @remarks
 * This function serves as a type guard which asserts the presence of the key, if found
 *
 * @typeParam T - The type of the object being searched
 * @typeParam K - The key being searched for
 *
 * @param o - The object being searched
 * @param k - The key being searched for
 *
 * @returns True if the key was found
 */
export function hasOwn<T, K extends PropertyKey>(o: T, k: K): o is Ensure<T, K> {
    return Object.prototype.hasOwnProperty.call(o, k);
}
