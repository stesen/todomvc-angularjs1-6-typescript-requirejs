import angular from "angular";
import {BehaviorSubject, Observable, Subject} from "rxjs";

import {shallowEquals} from "./object-utils";
import {AtLeastOne} from "./type-utils";

export interface Patch<T, K> {
    add?: T[];
    edit?: [K, Partial<T>][];
    delete?: K[];
}

export type Diff<T> = AtLeastOne<{
    before: Readonly<T>;
    after: Readonly<T>;
}>;

export default class PatchableDataSet<T extends object, K extends keyof T>
{
    private data: T[];
    private map = new Map<T[K], {index: number, value: T}>();
    private readonly dataSubject: BehaviorSubject<readonly Readonly<T>[]>;

    private readonly allChangesSubject: Subject<Diff<T>> = new Subject();
    private readonly propChangeSubjects = new Map<
        keyof T,
        {subject: Subject<Diff<T>>, refCount: number}
    >();

    public get observable(): Observable<readonly Readonly<T>[]> {
        return this.dataSubject.asObservable();
    }

    public get size(): number {
        return this.data.length;
    }

    constructor(public readonly primaryKey: K, initData?: T[]) {
        this.data = [];
        if (initData) {
            for (const item of initData) {
                this.pushItem(item);
            }
        }

        this.dataSubject = new BehaviorSubject<readonly Readonly<T>[]>(this.data);
    }

    private broadcastDataChange(): void {
        this.dataSubject.next(this.data);
    }

    private broadcastItemChange(diff: Diff<T>): void {
        this.allChangesSubject.next(diff);

        this.propChangeSubjects.forEach(({subject}, key) => {
            let propHasChanged = false;
            if (!diff.before) {
                propHasChanged = key in diff.after!;
            }
            else if (!diff.after) {
                propHasChanged = key in diff.before!;
            }
            else {
                propHasChanged = key in diff.after! !== key in diff.before!
                    || diff.after![key] !== diff.before![key];
            }

            if (propHasChanged) {
                subject.next(diff);
            }
        });
    }

    private pushItem(item: T): void {
        this.data.push(item);
        this.map.set(item[this.primaryKey], {index: this.data.length - 1, value: item});
    }

    private _add(items: T[]): void {
        for (const item of items) {
            if (this.map.has(item[this.primaryKey])) {
                const existingTuple = this.map.get(item[this.primaryKey])!;
                const {index, value: old} = existingTuple;
                this.data[index] = existingTuple.value = item;
                this.broadcastItemChange({before: old, after: item});
            }
            else {
                this.pushItem(item);
                this.broadcastItemChange({after: item});
            }
        }
    }

    private _edit(edits: [T[K], Partial<T>][]): void {
        for (const [key, partial] of edits) {
            if (!this.map.has(key)) throw new Error(`Cannot edit item at nonexistent key ${key}`);

            const {value} = this.map.get(key)!;
            const old = {...value};
            angular.extend(value, partial);
            this.broadcastItemChange({before: old, after: value});
        }
    }

    private _delete(keys: T[K][]): void {
        for (const key of keys) {
            if (this.map.has(key)) {
                const {index, value: old} = this.map.get(key)!;

                this.map.delete(key);
                this.data.splice(index, 1);

                for (let i = index; i < this.data.length; i++) {
                    const item = this.data[i];
                    this.map.get(item[this.primaryKey])!.index = i;
                }

                this.broadcastItemChange({before: old});
            }
        }
    }

    public replaceAll(data: T[]): void {
        const oldMap = this.map;
        this.data = [];
        this.map = new Map();
        for (const item of data) {
            this.pushItem(item);

            const newKey = item[this.primaryKey];
            if (oldMap.has(newKey)) {
                const oldItem = oldMap.get(newKey)!.value;
                if (!shallowEquals(item, oldItem)) {
                    this.broadcastItemChange({before: oldItem, after: item});
                }

                oldMap.delete(newKey);
            }
            else {
                this.broadcastItemChange({after: item});
            }
        }

        oldMap.forEach(({value: item}) => this.broadcastItemChange({before: item}));

        this.broadcastDataChange();
    }

    public add(items: T[]): void {
        this._add(items);
        this.broadcastDataChange();
    }

    public edit(edits: [T[K], Partial<T>][]): void {
        this._edit(edits);
        this.broadcastDataChange();
    }

    public delete(keys: T[K][]): void {
        this._delete(keys);
        this.broadcastDataChange();
    }

    public applyPatch(patch: Patch<T, T[K]>): void {
        if (patch.add) {
            this._add(patch.add);
        }

        if (patch.edit) {
            this._edit(patch.edit);
        }

        if (patch.delete) {
            this._delete(patch.delete);
        }

        this.broadcastDataChange();
    }

    public watch(prop?: keyof T): Observable<Diff<T>> {
        if (!prop) return this.allChangesSubject.asObservable();

        return new Observable(observer => {
            if (!this.propChangeSubjects.has(prop)) {
                this.propChangeSubjects.set(prop, {subject: new Subject(), refCount: 0});
            }

            const subjectTuple = this.propChangeSubjects.get(prop)!;
            subjectTuple.refCount++;
            const internalSubscription = subjectTuple.subject.subscribe(observer);

            return () => {
                internalSubscription.unsubscribe();

                if (--subjectTuple.refCount === 0) {
                    this.propChangeSubjects.delete(prop);
                }
            };
        });
    }
}
