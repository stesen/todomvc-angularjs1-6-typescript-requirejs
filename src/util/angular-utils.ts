import {hasOwn, keyOf} from "~/util/object-utils";
import {Omit} from "./type-utils";

/*
    This file contains helpers for creating a consistent, declarative experience using
    AngularJS with TypeScript.

    The general pattern is to have an "options" object, a decorated class,
    and a function that combines the two (whose return value is exported).
*/

/* DECORATORS */

/**
 * Marks a class as being injectable through the AngularJS DI container
 *
 * @param dependencies - The registered names of any injectables this class depends on
 */
export const injectable = (...dependencies: string[]) =>
    (clazz: new (...params: any) => any): void => {
        clazz.$inject = dependencies;
    };

/**
 * Marks an instance field as being an AngularJS binding
 *
 * @param bindingType - The AngularJS binding expression (a binding type symbol and optional name)
 */
export const binding = (bindingType: string) => (target: any, key: string): void => {
    if (typeof target === "function") throw new TypeError("Cannot bind to a static property");

    target.constructor.$bindings = target.constructor.$bindings || {};
    target.constructor.$bindings[key] = bindingType;
};

/* IMPORT HELPER */

/**
 * Helper for importing AngularJS modules. Will automatically get the name of any modules passed in.
 *
 * @param imports - The modules to import
 *
 * @returns An array of module names
 */
export const ngImports = (...imports: (ng.IModule | string)[]): string[] =>
    imports.map(module => typeof module === "string" ? module : module.name);

/* COMPONENT COMBINING FUNCTION */

/**
 * Helper for creating a complete component definition from an options object and controller class
 *
 * @param opts - The static component options
 * @param controllerClass - The controller class for the component
 *
 * @returns An AngularJS component registration object
 */
export function component(
    opts: ng.INamedComponentOptions,
    controllerClass: new (...params: any) => ng.IController
): {[registeredName: string]: ng.INamedComponentOptions} {
    if (controllerClass.$inject) {
        opts.$inject = controllerClass.$inject;
        delete controllerClass.$inject;
    }

    if (hasOwn(controllerClass, "$bindings")) {
        opts.bindings = controllerClass.$bindings as {[key: string]: string};
        delete controllerClass.$bindings;
    }

    opts.controller = controllerClass;

    return {[opts.registeredName]: opts};
}

/* DIRECTIVE DEFINITION HELPERS */

export type IDirectiveOptions = Omit<ng.IDirective, "compile" | "controller" | "link"> & {
    registeredName: string
};

/**
 * A controller defining the complete lifecycle of an AngularJS directive
 *
 * @typeParam TController - The type of the controller argument passed to the linking methods
 * @typeParam TAttributes - The type of the attributes object accepted by this directive
 * @typeParam TScope - The type of the scope used by this directive
 */
export interface IDirectiveLifecycleController<
    TController extends ng.IDirectiveController = ng.IController,
    TAttributes extends ng.IAttributes = ng.IAttributes,
    TScope extends ng.IScope = ng.IScope
> extends ng.IController {
    /**
     * An object containing any state shared between all instantiations of a particular
     * compiled instance of this directive
     */
    shared?: object;

    /**
     * The compilation function for this directive. Will be called from the `compile` hook.
     *
     * @param templateElement - The template element being compiled
     * @param templateAttributes - The attributes of the template element
     */
    $doCompile?(templateElement: JQLite, templateAttributes: TAttributes): void;

    /**
     * The pre-link function for this directive.
     * Will be called from the `pre` hook returned by `compile`.
     */
    $preLink?: ng.IDirectiveLinkFn<TScope, JQLite, TAttributes, TController>;

    /**
     * The post-link function for this directive.
     * Will be called from the `post` hook returned by `compile`.
     *
     * @remarks
     * Not to be confused with {@link ng.IController.$postLink}, which is called after both
     * of the directive-level linking functions.
     */
    $link?: ng.IDirectiveLinkFn<TScope, JQLite, TAttributes, TController>;
}

/**
 * Helper for creating a complete directive definition from an options object and controller class
 *
 * @typeParam TController - The type of the controller argument passed to the linking methods
 * @typeParam TAttributes - The type of the attributes object accepted by this directive
 * @typeParam TScope - The type of the scope used by this directive
 *
 * @param opts - The static directive options
 * @param controllerClass - The controller class for the directive
 *
 * @returns An AngularJS directive registration object
 */
export function directive<
    TController extends ng.IDirectiveController = ng.IController,
    TAttributes extends ng.IAttributes = ng.IAttributes,
    TScope extends ng.IScope = ng.IScope
>(
    opts: IDirectiveOptions,
    controllerClass: new (...params: any) => IDirectiveLifecycleController<
        TController,
        TAttributes,
        TScope
    >
): {[registeredName: string]: ng.IDirectiveFactory<TScope, JQLite, TAttributes, TController>} {
    //Declare a function to extract the controller for this directive
    //from the ctrls argument passed to the linking functions.
    //Have to use any for the param: the relationship between these different types is too dynamic.
    let extractThisCtrl: (ctrls: any) => [
        TController,
        IDirectiveLifecycleController<TController, TAttributes, TScope>
    ];

    //We also have to make sure that the controller for this directive will be passed in at all
    if (!opts.require || opts.require === opts.registeredName) {
        extractThisCtrl = ctrls => [ctrls, ctrls];
    }
    else if (typeof opts.require === "string") {
        opts.require = [opts.require, opts.registeredName];
        extractThisCtrl = ([requiredCtrl, thisCtrl]) => [requiredCtrl, thisCtrl];
    }
    else if (Array.isArray(opts.require)) {
        let thisIndex = opts.require.indexOf(opts.registeredName);

        if (thisIndex === -1) {
            opts.require.push(opts.registeredName);
            thisIndex = opts.require.length - 1;
        }

        extractThisCtrl = ctrls => [ctrls, ctrls[thisIndex]];
    }
    else if (typeof opts.require === "object") {
        let thisKey = keyOf(opts.require, opts.registeredName);

        if (!thisKey) {
            opts.require[opts.registeredName] = opts.registeredName;
            thisKey = opts.registeredName;
        }

        extractThisCtrl = ctrls => [ctrls, ctrls[thisKey!]];
    }

    const factory = (...ngDeps: any[]) => {
        const defn = opts as ng.IDirective<TScope, JQLite, TAttributes, TController>;
        defn.controller = function() { return new controllerClass(...ngDeps); };

        if (hasOwn(controllerClass, "$bindings")) {
            opts.bindToController = controllerClass.$bindings as {[key: string]: string};
            delete controllerClass.$bindings;
        }

        defn.compile = (templateElement: JQLite, templateAttributes: TAttributes) => {
            const compileController = new controllerClass(...ngDeps);

            if (compileController.$doCompile) {
                compileController.$doCompile(templateElement, templateAttributes);
            }

            const shared = compileController.shared;

            return {
                pre($scope, $el, $attrs, ctrls, $transclude) {
                    //To make TypeScript happy
                    if (!ctrls) throw new Error("Controllers should be defined");

                    const [ctrlsToPass, thisCtrl] = extractThisCtrl(ctrls);

                    thisCtrl.shared = shared;

                    if (thisCtrl.$preLink) {
                        thisCtrl.$preLink($scope, $el, $attrs, ctrlsToPass, $transclude);
                    }
                },

                post($scope, $el, $attrs, ctrls, $transclude) {
                    //To make TypeScript happy
                    if (!ctrls) throw new Error("Controllers should be defined");

                    const [ctrlsToPass, thisCtrl] = extractThisCtrl(ctrls);

                    if (thisCtrl.$link) {
                        thisCtrl.$link($scope, $el, $attrs, ctrlsToPass, $transclude);
                    }
                }
            };
        };

        return defn;
    };

    if (controllerClass.$inject) {
        factory.$inject = controllerClass.$inject;
        delete controllerClass.$inject;
    }

    return {[opts.registeredName]: factory};
}

/* SERVICE HELPERS */

/**
 * Interface describing a service class with a registeredName field
 */
export interface INamedServiceClass {
    new (...params: any): any;

    /**
     * The registered name of the service in the AngularJS DI container
     */
    readonly registeredName: string;
}

/**
 * Helper for creating an AngularJS service registration object
 *
 * @param serviceClass - The service class to register
 *
 * @returns A service registration object
 */
export const service = (serviceClass: INamedServiceClass) => ({
    [serviceClass.registeredName]: serviceClass
});
