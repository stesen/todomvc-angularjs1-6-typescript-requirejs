//For backwards compatibility reasons, you need this
//so that TypeScript sees this file as a module that is allowed to declare globals
export {};

//Augmented global types
declare global {
    //Define IE-compatible versions of Map and WeakMap
    //(The same could be done for (Weak)Sets)

    interface IECompatibleWeakMap<K extends object, V> {
        set(key: K, value: V): void;
        has(key: K): boolean;
        get(key: K): V | undefined;
        delete(key: K): boolean;
    }
    const WeakMap: new <K extends object, V>() => IECompatibleWeakMap<K, V>;

    interface IECompatibleMap<K, V> {
        readonly size: number;

        clear(): void;
        delete(key: K): boolean;
        forEach<TThis>(
            callback: (this: TThis, value: V, key: K, map: IECompatibleMap<K, V>) => void,
            thisArg?: TThis
        ): void;
        get(key: K): V | undefined;
        has(key: K): boolean;
        set(key: K, value: V): void;
    }
    const Map: new <K, V>() => IECompatibleMap<K, V>;

    //Since the AngularJS types are declared as globals, we can augment them
    namespace ng {
        interface IComponentOptions {
            //For some reason this isn't included in the type definitions
            $inject?: readonly string[];
        }

        //This is a separate type and not just included in the above augmentation
        //because I didn't want to make it required on all component definitions,
        //but I also didn't want to have to make a non-null assertion every time the
        //registeredName was referenced.
        //I left it in the ng namespace because it still basically is just an augmentation.
        interface INamedComponentOptions extends IComponentOptions {
            registeredName: string;
        }
    }
}
