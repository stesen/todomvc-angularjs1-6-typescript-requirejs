"use strict";

var require = {
    baseUrl: ".",
    //This makes the .js extension at the end of paths optional
    nodeIdCompat: true,
    paths: {
        "~": "dist", //Allow referring to the root of the project as "~"
        "tslib": "node_modules/tslib/tslib",
        "angular": "node_modules/angular/angular",
        "angular-localforage": "node_modules/angular-localforage/dist/angular-localForage",
        "rxjs": "node_modules/rxjs/bundles/rxjs.umd",
        "rxjs/operators": "lib/rxjs/operators"
    },
    shim: {
        "angular": {exports: "angular"},
        //angular-localforage is weird in that it imports localforage as a module,
        //but expects angular to be a global
        "angular-localforage": {deps: ["angular"]}
    },
    //This is like paths, but for specific modules. That is to say,
    //map["angular-localforage"] defines paths that only affect the imports
    //angular-localforage makes in its own code.
    //This is being used to tell modules where their dependencies are.
    map: {
        "angular-localforage": {
            "localforage": "node_modules/localforage/dist/localforage",
        },
        "localforage": {
            "lie/polyfill": "node_modules/lie/dist/lie.polyfill"
        },
        "lie/polyfill": {
            "immediate": "node_modules/immediate/dist/immediate"
        }
    }
};
