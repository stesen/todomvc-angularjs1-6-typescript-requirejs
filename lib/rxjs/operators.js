/*
    This file exists because the RxJS type definitions disagree with the UMD build:
    The type definitions put the operators in their own rxjs/operators submodule,
    however the UMD build exports everything under a single namespace object.
    This leads to a disconnect between the import that TypeScript understands and the import
    that works at runtime. The solution was to create this plain-JS shim file and map
    "rxjs/operators" to it in the RequireJS config file.
*/

define([
    "rxjs"
], function(
    Rx
) {
    "use strict";

    return Rx.operators;
});
